﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinformControlLibraryExtension;

namespace Test
{
    public partial class Test : Form
    {
        public Test()
        {
            InitializeComponent();
        }

        private void Test_Load(object sender, EventArgs e)
        {
            this.imageCarouselExt1.Play();
        }

        private void Test_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Environment.Exit(0);
        }


        private void imageCarouselExt1_NavigationBarBtnClick(object sender, WinformControlLibraryExtension.ImageCarouselExt.NavigationBarBtnClickEventArgs e)
        {
           
        }


        int i = 1;          //用来存放图片索引的
        private void imageCarouselExt1_Click(object sender, EventArgs e)
        {
           
            //获取当前图片的文本（可以当做歌星的编号，然后根据编号查询）
            string text = ((ImageCarouselExt)sender).Images[i-1].Text;
            MessageBox.Show(text.ToString());

        }

        private void imageCarouselExt1_ImageFrameIndexChanged(object sender, ImageCarouselExt.ImageFrameIndexChangedEventArgs e)
        {
            //获取当前图片的索引
            i = e.index+1;
           
        }
    }
}
