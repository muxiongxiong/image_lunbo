﻿namespace Test
{
    partial class Test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            WinformControlLibraryExtension.ImageCarouselExt.ImageItem imageItem1 = new WinformControlLibraryExtension.ImageCarouselExt.ImageItem();
            WinformControlLibraryExtension.ImageCarouselExt.ImageItem imageItem2 = new WinformControlLibraryExtension.ImageCarouselExt.ImageItem();
            WinformControlLibraryExtension.ImageCarouselExt.ImageItem imageItem3 = new WinformControlLibraryExtension.ImageCarouselExt.ImageItem();
            this.imageCarouselExt1 = new WinformControlLibraryExtension.ImageCarouselExt();
            this.SuspendLayout();
            // 
            // imageCarouselExt1
            // 
            this.imageCarouselExt1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.imageCarouselExt1.ImageFrameHeight = 400;
            imageItem1.Image = global::Test.Properties.Resources._1;
            imageItem1.Text = "1";
            imageItem2.Image = global::Test.Properties.Resources._2;
            imageItem2.Text = "2";
            imageItem3.Image = global::Test.Properties.Resources._3;
            imageItem3.Text = "3";
            this.imageCarouselExt1.Images.Add(imageItem1);
            this.imageCarouselExt1.Images.Add(imageItem2);
            this.imageCarouselExt1.Images.Add(imageItem3);
            this.imageCarouselExt1.Location = new System.Drawing.Point(73, 38);
            this.imageCarouselExt1.Name = "imageCarouselExt1";
            this.imageCarouselExt1.Size = new System.Drawing.Size(400, 364);
            this.imageCarouselExt1.TabIndex = 0;
            this.imageCarouselExt1.ImageFrameIndexChanged += new WinformControlLibraryExtension.ImageCarouselExt.ImageFrameEventHandler(this.imageCarouselExt1_ImageFrameIndexChanged);
            this.imageCarouselExt1.NavigationBarBtnClick += new WinformControlLibraryExtension.ImageCarouselExt.NavigationBarBtnClickEventHandler(this.imageCarouselExt1_NavigationBarBtnClick);
            this.imageCarouselExt1.Click += new System.EventHandler(this.imageCarouselExt1_Click);
            // 
            // Test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 568);
            this.Controls.Add(this.imageCarouselExt1);
            this.Name = "Test";
            this.Text = "Test";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Test_FormClosing);
            this.Load += new System.EventHandler(this.Test_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private WinformControlLibraryExtension.ImageCarouselExt imageCarouselExt1;

    }
}